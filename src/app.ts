import express from "express";
import mysql from "mysql";
import os from "os";

const app = express();
const port = 8080;
const version = "1.0.0";

let dbStatus = "";

// Environment variables for MySQL configuration
const dbConfig: mysql.ConnectionConfig = {
  host: process.env.DB_HOST || "dbk8s-1.mysql.database.azure.com",
  user: process.env.DB_USERNAME || "m210",
  password: process.env.DB_PASSWORD || "root1234!",
  database: process.env.DB_NAME || "hellom210",
};

// Falls Ctrl-C gedrückt oder Exit-Signal gesendet wird soll Applikation beenden.

function exitHandle(signal: Event): void {
  console.log(`Exiting because of ${signal}`);
  process.exit(0);
}

process.on("SIGINT", exitHandle);
process.on("SIGTERM", exitHandle);

// MySQL connection
const db = mysql.createConnection(dbConfig);
db.connect((err) => {
  dbStatus = err
    ? `Error: No Connection to ${dbConfig.host}.`
    : `Success: Connected to ${dbConfig.database} on ${dbConfig.host}.`;
});

// Routes
app.get("/", (req, res) => {
  res.send(
    `<h1>Hellom210</h1>
    <strong>Version:</strong> ${version}<br> 
    <strong>Hostname:</strong> ${os.hostname()}<br>
    <strong>DB Status:</strong> ${dbStatus}`
  );
});

// Start server
app.listen(port, () => {
  console.log(
    `Server running on http://localhost:${port}! Press Ctrl-C to Exit.`
  );
});
